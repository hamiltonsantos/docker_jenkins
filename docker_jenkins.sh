#/bin/bash

## docker_jenkins.sh - Instalar Docker 
## e atualizar permissões para o usuario jenkins
## Escrito por: Hernani Soares (NIteroi - RJ)
## E-mail: soaresnetoh@gmail.com
## Linkedin: https://www.linkedin.com/in/soaresnetoh/
## Shell script / Jenkins 

# Exemplo de uso: 
# $ chmod +x docker_jenkins.sh
# $ ./docker_jenkins.sh
# 
# Script desenvolvido para sanar problema quando no Jenkins, 
# se tenta usar o docker e recebe o erro de permissão negada
# na pasra /var/run/docker.sock

echo "\n\nAtualizando Lista de pacotes\n\n"
sudo apt-get update

echo "\n\nInstalando Docker - docker.io\n\n"
sudo apt-get install docker.io -y

# Uma ACL (Access Control List / Lista de Controle de Acesso) 
# é uma configuração de segurança que nos fornece um controle 
# mais refinado sobre quais usuários podem acessar diretórios 
# e arquivos específicos do que as permissões tradicionais do 
# Linux.

echo -e "\n\nAtualizando permissões em /var/run/docker.sock para usuario jenkins\n\n"
sudo setfacl --modify user:jenkins:rw /var/run/docker.sock

# Caso queira remover esta permissão:
# sudo setfacl -x u:jenkins /var/run/docker.sock
