# Docker Jenkins

Script shell para sanar problema de permissão na pasta /var/run/docker.sock

Script: docker_jenkins.sh 
Instalar Docker e atualizar permissões para o usuario jenkins

Escrito por: Hernani Soares (NIteroi - RJ)
E-mail: soaresnetoh@gmail.com
Linkedin: https://www.linkedin.com/in/soaresnetoh/
Shell script / Jenkins 

Exemplo de uso: 
$ chmod +x docker_jenkins.sh
$ ./docker_jenkins.sh
 
Script desenvolvido para sanar problema quando no Jenkins, 
se tenta usar o docker e recebe o erro de permissão negada
na pasra /var/run/docker.sock
